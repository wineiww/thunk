import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './App.css'
import { fetchPokemon, RootState } from './store'

const App = () => {
	const dispatch = useDispatch()
	const { data, isLoading } = useSelector((state: RootState) => state.pokemon)

	const _fetchPokemon = React.useCallback(async () => {
		dispatch(fetchPokemon())
	}, [dispatch])

	React.useEffect(() => {
		_fetchPokemon()
	}, [_fetchPokemon])

	return (
		<div className='container'>
			{isLoading && <h1>Loading...</h1>}
			<ul className='list-wrapper'>
				{data.map((pokemon) => (
					<li className='pokemon-wrapper' key={pokemon.name}>
						{pokemon.name}
					</li>
				))}
			</ul>
		</div>
	)
}

export default App

import {
	FetchPokemonTypes,
	IPokemon,
	POKEMON_FETCH_PENDING,
	POKEMON_FETCH_SUCCESS,
} from './types'

export interface IPokemonState {
	data: IPokemon[]
	isLoading: boolean
}

const initialState: IPokemonState = {
	data: [],
	isLoading: false,
}

export const pokemonReducer = (
	state = initialState,
	action: FetchPokemonTypes
): IPokemonState => {
	switch (action.type) {
		case POKEMON_FETCH_PENDING:
			return {
				...state,
				isLoading: true,
			}
		case POKEMON_FETCH_SUCCESS:
			return {
				...state,
				isLoading: false,
				data: action.payload,
			}
		default:
			return state
	}
}

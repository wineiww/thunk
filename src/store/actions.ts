import { pokemonApi } from './api'
import {
	AppThunk,
	FetchPokemonTypes,
	IPokemon,
	POKEMON_FETCH_PENDING,
	POKEMON_FETCH_SUCCESS,
} from './types'

export const fetchPokemon = (): AppThunk => async (dispatch) => {
	dispatch(fetchPokemonPending())
	const data = await pokemonApi.fetchPokemon()
	setTimeout(() => {
		return dispatch(fetchPokemonSuccess(data))
	}, 2000) // This is just to show async operation
}

export const fetchPokemonPending = (): FetchPokemonTypes => ({
	type: POKEMON_FETCH_PENDING,
})

export const fetchPokemonSuccess = (
	payload: IPokemon[]
): FetchPokemonTypes => ({
	type: POKEMON_FETCH_SUCCESS,
	payload,
})

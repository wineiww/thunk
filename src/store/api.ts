const URL = 'https://pokeapi.co/api/v2/pokemon?limit=100'

const fetchPokemon = async () => {
	const res = await fetch(URL)
	const { results } = await res.json()
	return results
}

export const pokemonApi = {
	fetchPokemon,
}

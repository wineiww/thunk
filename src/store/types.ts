import { Action } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { RootState } from '.'

export const POKEMON_FETCH = '@POKEMON/FETCH'
export const POKEMON_FETCH_PENDING = '@POKEMON/FETCH_PENDING'
export const POKEMON_FETCH_SUCCESS = '@POKEMON/FETCH_SUCCESS'

export interface IPokemon {
	name: string
	url: string
}

interface IFetchPokemonAction {
	type: typeof POKEMON_FETCH_PENDING
}

interface IFetchPokemonSuccessAction {
	type: typeof POKEMON_FETCH_SUCCESS
	payload: IPokemon[]
}

export type FetchPokemonTypes = IFetchPokemonAction | IFetchPokemonSuccessAction

export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>

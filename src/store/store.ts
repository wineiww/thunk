import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import { pokemonReducer } from './reducers'

const rootReducer = combineReducers({
	pokemon: pokemonReducer,
})

export type RootState = ReturnType<typeof rootReducer>

export const configureStore = () => {
	const store = createStore(rootReducer, applyMiddleware(thunk))

	return store
}
